module.exports = (req, res, next) => {
  res.status(404).send({ error: 'Page not found' });
  next();
}

#!/bin/bash

docker login --username=_ --password=$HEROKU_API_KEY registry.heroku.com
echo -n "$HEROKU_CREDENTIALS" > ~/.netrc

docker pull $GITLAB_IMAGE_NAME
docker tag $GITLAB_IMAGE_NAME $HEROKU_IMAGE_NAME
docker push $HEROKU_IMAGE_NAME

heroku container:release web --app $HEROKU_APP_NAME

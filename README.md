# numerical-metrics

## API Documentation

### Heartbeat

* Route: GET `/api/heartbeat`
* Response:
    * Status:
        * 200 if the service is available
    * Body:

```json
{
  "status": "available"
}
```

### Metrics

* Route: GET `/api/metrics`
* Request:
    * Headers:
        * Authorization: Authentication token
    * Body:

```json
{
  "weight": 100.0,
  "weightUnits": "kg", // Either kg or lb
  "bodyFatPercentage": 15.2
}
```

* Response:
    * Status:
        * 201 if the metrics are successfully saved
        * 400 otherwise
    * Body: None

const express = require('express');
const cors = require('cors');
const basicAuth = require('express-basic-auth');
const bodyParser = require('body-parser');

const config = require('./config');
const notFoundParser = require('./middlewares/notFoundParser');
const basicAuthHelper = require('./middlewares/basicAuthHelper');

//
// Dependencies
//

const basicAuthMiddleware = basicAuth({
  authorizer: basicAuthHelper.validateCredentials,
  unauthorizedResponse: basicAuthHelper.unauthorizedResponse
});

const PountToKgConverter = require('./helpers/poundToKgConverter');
const MetricsService = require('./services/metrics');

const metricsService = new MetricsService(new PountToKgConverter());

const heartbeat = require('./routes/heartbeat')();
const metrics = require('./routes/metrics')(metricsService);

//
// Routes
//

const router = express.Router();
router.use('/heartbeat', heartbeat);
router.use('/metrics', basicAuthMiddleware, metrics);

//
// Express server
//

const app = express();
app.disable('x-powered-by');
app.use(bodyParser.json());
app.use(config.web.baseUrl, router);
app.use(cors(config.web.corsOptions));
app.use(notFoundParser);

app.listen(config.web.port, () => {
  console.log('Server started listening on port ' + config.web.port);
});

//
// Background tasks
//

require('./helpers/db');  // Creates a DB connection

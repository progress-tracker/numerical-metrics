const mongoose = require('mongoose');

var metricsSchema = new mongoose.Schema(
  {
    weightInKg: Number,
    bodyFatPercentage: Number,
    timestamp: {
      type: Date,
      index: true
    }
  },
  { collection: 'metrics' }
);


module.exports = exports = mongoose.model('Metric', metricsSchema);

FROM node:latest

ENV NODE_ENV production
ENV APP_DIRECTORY /opt/numerical-metrics

ADD . ${APP_DIRECTORY}
WORKDIR ${APP_DIRECTORY}

RUN npm install

CMD [ "/bin/sh", "scripts/start_server.sh" ]

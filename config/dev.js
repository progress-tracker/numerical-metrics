const prod = require('./prod');

// Create a deep copy
var config = JSON.parse(JSON.stringify(prod));

config.web.port = 3000;
config.web.basicAuthUsername = 'admin';
config.web.basicAuthPassword = 'admin';

config.db.uri = 'mongodb://localhost:27017'

module.exports = config;

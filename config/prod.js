var config = {};

config.web = {};
config.web.port = process.env.PORT;
config.web.baseUrl = '/api';
config.web.basicAuthUsername = process.env.BASIC_AUTH_USERNAME;
config.web.basicAuthPassword = process.env.BASIC_AUTH_PASSWORD;
config.web.corsOptions = {
  origin: '*',
  methods: ['GET', 'POST'],
};

config.db = {};
config.db.uri = process.env.MONGODB_URI

module.exports = config;

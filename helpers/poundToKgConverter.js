const weightUnits = Object.freeze({
  'kg': 1,
  'lb': 2
});

class PoundToKgConverter {
  toKg(weight, units) {
    switch (units.toLowerCase()) {
      case 'kg':
        return weight;
      case 'lb':
        return weight / 2.2;
      default:
        throw new Error('unknown weight unit');
    }
  }
}

module.exports = PoundToKgConverter;

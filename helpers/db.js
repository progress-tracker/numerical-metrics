const mongoose = require('mongoose');
const config = require('../config');

mongoose.connect(config.db.uri + '/pt-numerical-metrics', { useNewUrlParser: true, autoReconnect: true });

mongoose.connection.on('connect', () => {
  console.log('Connected to MongoDB');
});

mongoose.connection.on('disconnect', () => {
  console.log('Disconnected from MongoDB');
});

mongoose.connection.on('error', () => {
  console.log('An error occurred while using MongoDB');
});

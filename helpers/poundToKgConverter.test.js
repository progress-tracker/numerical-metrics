const PoundToKgConverter = require('./poundToKgConverter');

const ORIGINAL_WEIGHT = 10;

describe('toKg', () => {
  it('returns original value when weight in kg', () => {
    var poundToKgConverter = new PoundToKgConverter();

    var convertedWeight = poundToKgConverter.toKg(ORIGINAL_WEIGHT, 'kg');

    expect(convertedWeight).toBe(ORIGINAL_WEIGHT);
  });

  it('returns original value when weight in pounds', () => {
    var poundToKgConverter = new PoundToKgConverter();

    var convertedWeight = poundToKgConverter.toKg(ORIGINAL_WEIGHT, 'lb');

    expect(convertedWeight).toBe(ORIGINAL_WEIGHT / 2.2);
  });

  it('returns an error when unknown weight unit', () => {
    var poundToKgConverter = new PoundToKgConverter();

    try {
      poundToKgConverter.toKg(ORIGINAL_WEIGHT, 'UNKNOWN');
      fail();
    } catch (err) {
      expect(err.message).toBe('unknown weight unit');
    }
  });
});

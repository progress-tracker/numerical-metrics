const Metrics = require('../models/metrics');

class MetricsService {
  constructor(pountToKgConverter) {
    this.pountToKgConverter = pountToKgConverter;
  }

  async create(weight, weightUnits, bodyFatPercentage, date) {
    if (!weight || weight < 0) {
      return Promise.reject(new Error('invalid weight'));
    }

    if (!weightUnits) {
      return Promise.reject(new Error('invalid weightUnits'));
    }

    if (!bodyFatPercentage || bodyFatPercentage < 0) {
      return Promise.reject(new Error('invalid bodyFatPercentage'));
    }

    if (!date) {
      return Promise.reject(new Error('invalid date'));
    }

    try {
      var metricsDocument = {
        weightInKg: this.pountToKgConverter.toKg(weight, weightUnits),
        bodyFatPercentage: bodyFatPercentage,
        timestamp: date
      }

      await Metrics.create(metricsDocument);
      console.log('Successfully created metrics');

      return Promise.resolve();
    } catch (err) {
      var message = 'unable to create metrics: ' + err.message;
      console.log(message);
      return Promise.reject(new Error(message));
    }
  }
}

module.exports = MetricsService;

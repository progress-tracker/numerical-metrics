const MetricsService = require('./metrics');
const Metrics = require('../models/metrics');
const PoundToKgConverter = require('../helpers/poundToKgConverter');
jest.mock('../helpers/poundToKgConverter');

const WEIGHT = 100.0;
const WEIGHT_UNIT = 'kg';
const BF_PERCENTAGE = 15.0;
const DATE = new Date();

beforeEach(() => PoundToKgConverter.mockClear());

describe('create', () => {
  it('persists metrics document when valid creation request', async () => {
    Metrics.create = jest.fn(() => { });
    PoundToKgConverter.mockImplementation(() => {
      return {
        toKg: (kg) => kg
      }
    });
    var metricsService = new MetricsService(new PoundToKgConverter());

    await metricsService.create(WEIGHT, WEIGHT_UNIT, BF_PERCENTAGE, DATE);

    var expectedMetricsDocument = {
      weightInKg: WEIGHT,
      bodyFatPercentage: BF_PERCENTAGE,
      timestamp: DATE
    };
    expect(Metrics.create).toHaveBeenCalledTimes(1);
    expect(Metrics.create).toHaveBeenCalledWith(expectedMetricsDocument)
  });

  it('returns an error when persistence fails', async () => {
    Metrics.create = jest.fn(() => { throw new Error('error') });
    PoundToKgConverter.mockImplementation(() => {
      return {
        toKg: (kg) => kg
      }
    });
    var metricsService = new MetricsService(new PoundToKgConverter());

    try {
      await metricsService.create(WEIGHT, WEIGHT_UNIT, BF_PERCENTAGE, DATE);
      fail('no error thrown');
    } catch (err) {
      expect(Metrics.create).toHaveBeenCalledTimes(1);
      expect(err.message).toBe('unable to create metrics: error');
    }
  });

  it('returns an error when no weight is provided', async () => {
    var metricsService = new MetricsService(new PoundToKgConverter());

    try {
      await metricsService.create(undefined, WEIGHT_UNIT, BF_PERCENTAGE, DATE);
      fail('no error thrown');
    } catch (err) {
      expect(err.message).toBe('invalid weight');
    }
  });

  it('returns an error when weight is negative', async () => {
    var metricsService = new MetricsService(new PoundToKgConverter());

    try {
      await metricsService.create(-1, WEIGHT_UNIT, BF_PERCENTAGE, DATE);
      fail('no error thrown');
    } catch (err) {
      expect(err.message).toBe('invalid weight');
    }
  });

  it('returns an error when no weightUnits is provided', async () => {
    var metricsService = new MetricsService(new PoundToKgConverter());

    try {
      await metricsService.create(WEIGHT, undefined, BF_PERCENTAGE, DATE);
      fail('no error thrown');
    } catch (err) {
      expect(err.message).toBe('invalid weightUnits');
    }
  });

  it('returns an error when no bodyFatPercentage is provided', async () => {
    var metricsService = new MetricsService(new PoundToKgConverter());

    try {
      await metricsService.create(WEIGHT, WEIGHT_UNIT, undefined, DATE);
      fail('no error thrown');
    } catch (err) {
      expect(err.message).toBe('invalid bodyFatPercentage');
    }
  });

  it('returns an error when bodyFatPercentage is negative', async () => {
    var metricsService = new MetricsService(new PoundToKgConverter());

    try {
      await metricsService.create(WEIGHT, WEIGHT_UNIT, -1, DATE);
      fail('no error thrown');
    } catch (err) {
      expect(err.message).toBe('invalid bodyFatPercentage');
    }
  });

  it('returns an error when no date is provided', async () => {
    var metricsService = new MetricsService(new PoundToKgConverter());

    try {
      await metricsService.create(WEIGHT, WEIGHT_UNIT, BF_PERCENTAGE, undefined);
      fail('no error thrown');
    } catch (err) {
      expect(err.message).toBe('invalid date');
    }
  });
});

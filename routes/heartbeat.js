const express = require('express');

module.exports = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    res.status(200).send({ status: 'available' });
  });

  return router;
}

const express = require('express');
const request = require('supertest');

const heartbeat = require('./heartbeat')();

function createApp() {
  const app = express();
  app.use('/heartbeat', heartbeat);
  return app;
}

describe('GET /heartbeat', () => {
  it('returns status available', (done) => {
    request(createApp())
      .get('/heartbeat')
      .expect(200)
      .then(response => {
        expect(response.body.status).toBe('available');
        done();
      });
  });
});



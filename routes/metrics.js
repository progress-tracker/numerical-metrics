const express = require('express');

module.exports = (metricsService) => {
  const router = express.Router();

  router.post('/', async (req, res) => {
    var weight = req.body.weight;
    var weightUnits = req.body.weightUnits;
    var bodyFatPercentage = req.body.bodyFatPercentage;

    if (!weight || !weightUnits || !bodyFatPercentage) {
      res.status(400).send({ error: 'missing parameter' });
      return;
    }

    if (typeof weight !== 'number' || typeof bodyFatPercentage !== 'number') {
      res.status(400).send({ error: 'invalid input' });
      return;
    }

    try {
      await metricsService.create(weight, weightUnits, bodyFatPercentage, new Date());
      res.status(201).send();
    } catch (err) {
      res.status(400).send({ error: err.message });
    }
  });

  return router;
};

const request = require('supertest');

const Metrics = require('../models/metrics');
const MetricsService = require('../services/metrics');
const PoundToKgConverter = require('../helpers/poundToKgConverter');
const metricsService = new MetricsService(new PoundToKgConverter());

const metrics = require('./metrics')(metricsService);

function createApp() {
  const app = require('express')();
  const bodyParser = require('body-parser');

  app.use(bodyParser.json());
  app.use('/api/metrics', metrics)

  return app;
}

const WEIGHT = 100.0;
const WEIGHT_UNITS = 'kg';
const BF_PERCENTAGE = 15.0;

describe('POST /api/metrics', () => {
  it('creates metrics when valid request', (done) => {
    Metrics.create = jest.fn(() => { });

    request(createApp())
      .post('/api/metrics')
      .send({ weight: WEIGHT, weightUnits: WEIGHT_UNITS, bodyFatPercentage: BF_PERCENTAGE })
      .expect(201)
      .then(_ => done());
  });

  it('returns bad request when no weight is provided', (done) => {
    request(createApp())
      .post('/api/metrics')
      .send({ weight: null, weightUnits: WEIGHT_UNITS, bodyFatPercentage: BF_PERCENTAGE })
      .expect(400)
      .then(response => {
        expect(response.body.error).toBe('missing parameter');
        done();
      });
  });

  it('returns bad request when no weightUnits is provided', (done) => {
    request(createApp())
      .post('/api/metrics')
      .send({ weight: WEIGHT, weightUnits: null, bodyFatPercentage: BF_PERCENTAGE })
      .expect(400)
      .then(response => {
        expect(response.body.error).toBe('missing parameter');
        done();
      });
  });

  it('returns bad request when no bodyFatPercentage is provided', (done) => {
    request(createApp())
      .post('/api/metrics')
      .send({ weight: WEIGHT, weightUnits: WEIGHT_UNITS, bodyFatPercentage: null })
      .expect(400)
      .then(response => {
        expect(response.body.error).toBe('missing parameter');
        done();
      });
  });

  it('returns bad request when weight is not a number', (done) => {
    request(createApp())
      .post('/api/metrics')
      .send({ weight: 'potato', weightUnits: WEIGHT_UNITS, bodyFatPercentage: BF_PERCENTAGE })
      .expect(400)
      .then(response => {
        expect(response.body.error).toBe('invalid input');
        done();
      });
  });

  it('returns bad request when bodyFatPercentage is not a number', (done) => {
    request(createApp())
      .post('/api/metrics')
      .send({ weight: WEIGHT, weightUnits: WEIGHT_UNITS, bodyFatPercentage: 'potato' })
      .expect(400)
      .then(response => {
        expect(response.body.error).toBe('invalid input');
        done();
      });
  });
});
